package com.mobile.tdsoftware;


import java.util.ArrayList;

public class LevenShtein {


    /* This is a solution of LevenShtein problem which have been answered by Iman Kazemini
     * Note: You can find more information on Wiki
     * There is different Levenshtein algorithm but these two methods use ONE column to reduce memory usage.
     * The second one uses a LIMITATION to terminate sooner
     */


    //Complexity:  BestCase: 1  AverageCase: N^2   WorstCase: N^2
    private int OneColumnLevenShteinDistance(String token1, String token2) {
        //return for zero length
        if (token1.length() == 0)
            return token2.length();
        if (token2.length() == 0)
            return token1.length();

        // define just one column to reduce space
        int token1Lenght = token1.length();
        int[] column = new int[token1Lenght + 1];

        int index2 = 0;
        char ch2 = token2.charAt(index2);
        column[0] = 1;

        //Initiate column and comparison base on virtual column
        for (int index1 = 1; index1 <= token1Lenght; index1++) {
            int cost = token1.charAt(index1 - 1) == ch2 ? 0 : 1;
            //Thinkng of a matrix we should calculate the Lowest of three: top (column[index1 - 1]), aboveleft: index1 - 1,
            // left: index1.
            column[index1] = Math.min(column[index1 - 1], index1 - 1) + cost;
        }

        // okay, now we have an initialized first column, and we can
        // compute the rest of a virtual matrix.
        int top = 0;
        for (index2 = 1; index2 < token2.length(); index2++) {
            ch2 = token2.charAt(index2);
            top = index2 + 1;

            int smallest = token1Lenght * 2;
            for (int ix1 = 1; ix1 <= token1Lenght; ix1++) {
                int cost = token1.charAt(ix1 - 1) == ch2 ? 0 : 1;

                int value = Math.min(Math.min(top, column[ix1 - 1]), column[ix1]) +
                        cost;
                column[ix1 - 1] = top;
                top = value;
                smallest = Math.min(smallest, value);
            }
            column[token1Lenght] = top;
        }
        return top;
    }




    //Complexity:  BestCase: 1  AverageCase: N^2   WorstCase: N^2
    private int OneColumnLevenShteinDistanceWithMaximum(String token1, String token2, int maxDist) {
        if (token1.length() == 0)
            return token2.length();
        if (token2.length() == 0)
            return token1.length();

        int token1Lenght = token1.length();
        int[] column = new int[token1Lenght + 1];

        int index2 = 0;
        char ch2 = token2.charAt(index2);
        column[0] = 1;

        for (int index1 = 1; index1 <= token1Lenght; index1++) {
            int cost = token1.charAt(index1 - 1) == ch2 ? 0 : 1;
            column[index1] = Math.min(column[index1 - 1], index1 - 1) + cost;
        }

        int top = 0;
        for (index2 = 1; index2 < token2.length(); index2++) {
            ch2 = token2.charAt(index2);
            top = index2 + 1;
            int minimum = token1Lenght * 2;
            for (int ix1 = 1; ix1 <= token1Lenght; ix1++) {
                int cost = token1.charAt(ix1 - 1) == ch2 ? 0 : 1;

                int value = Math.min(Math.min(top, column[ix1 - 1]), column[ix1]) +
                        cost;
                column[ix1 - 1] = top;
                top = value;
                minimum = Math.min(minimum, value);
            }
            column[token1Lenght] = top;

            //because of definition fo maximum, we have to return max, when minimum of calculation exceed the max
            if (minimum > maxDist)
                return maxDist + 1;
        }

        return top;
    }




}